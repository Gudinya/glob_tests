$(function () {
    $('#getstudents').click(function () {
        $('.custom-container').text('');
        $('.rmst').remove();
        $('#calcform').hide();
        $('#schema').hide();
        $('#result').hide();

        var student_info = $('.stud'),
            per = $('.period'),
            markrow = $('.mark-row');

        var result = 0;

        $.get("/gettranscription", function (data) {
            for (var stdid in data) {
                var new_student = student_info.clone();
                var courses_num = 0;

                new_student.addClass('rmst');
                new_student.find('.id').text(stdid);
                new_student.find('.name').text(data[stdid].details.st_name);
                new_student.find('.major').text(data[stdid].details.mjr_name);

                for (var period in data[stdid].courses_marks) {
                    var marks = new_student.find('.marks');
                    var new_period = per.clone();

                    new_period.find('.period-name').text(period);
                    new_period.show();
                    marks.append(new_period)

                    for (var mark in data[stdid].courses_marks[period]) {
                        var course = data[stdid].courses_marks[period][mark];
                        var new_markrow = markrow.clone();
                        new_markrow.find('.course').text(course['crs_name']);
                        new_markrow.find('.stmark').text(course['mark_name']);
                        new_markrow.show();
                        marks.append(new_markrow);
                        result = result + course['mark_point'];
                        courses_num = courses_num + 1;
                    }
                }
                new_student.find('.gpa').text(Math.round(result / courses_num * 100) / 100);
                new_student.show();
                $('.custom-container').append(new_student);
            }
        });
    });

    $('#showcalclist').click(function () {
        $('#calcform').show();
        $('#schema').hide();
        $('#result').hide();
        $('.rmst').remove();
    });

    $("#calcform").submit(function(event){
        event.preventDefault();
        $.get("/docalc", {in: String($('#numlist').val())}, function (data) {
            $('.custom-container').html('<span style="white-space:pre-wrap;">' + data + '</span>');
        });
    });

    $('#showschema').click(function () {
        $('.custom-container').text('');
        $('.rmst').remove();
        $('#calcform').hide();
        $('#schema').show();
        $('#result').hide();
    });

    $('#showresult').click(function () {
        $('.custom-container').text('');
        $('.rmst').remove();
        $('#calcform').hide();
        $('#schema').hide();
        $('#result').show();
    });


});