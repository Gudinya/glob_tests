#!/usr/bin/env python
import SocketServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
from urlparse import urlparse, parse_qs

import sqlite3
import os
import json

DB_FILE = 'test.db'

# recreate database on each run, to make db development easier
if os.path.exists(DB_FILE):
    os.remove(DB_FILE)
conn = sqlite3.connect(DB_FILE)

conn.execute("CREATE TABLE periods(per_id INTEGER, per_name char(50));")
conn.execute("INSERT INTO periods VALUES(1, 'Winter 2013');")
conn.execute("INSERT INTO periods VALUES(2, 'Fall 2012');")
conn.execute("INSERT INTO periods VALUES(3, 'Spring 2012');")

conn.execute("CREATE TABLE marks(mark_id INTEGER, mark_name char(2), mark_point REAL);")
conn.execute("INSERT INTO marks VALUES(1, 'A', '4.0');")
conn.execute("INSERT INTO marks VALUES(2, 'A-', '3.7');")
conn.execute("INSERT INTO marks VALUES(3, 'B+', '3.3');")
conn.execute("INSERT INTO marks VALUES(4, 'B', '3.0');")
conn.execute("INSERT INTO marks VALUES(5, 'B-', '2.7');")
conn.execute("INSERT INTO marks VALUES(6, 'C+', '2.3');")
conn.execute("INSERT INTO marks VALUES(7, 'C', '2.0');")
conn.execute("INSERT INTO marks VALUES(8, 'C-', '1.7');")
conn.execute("INSERT INTO marks VALUES(9, 'D+', '1.3');")
conn.execute("INSERT INTO marks VALUES(10, 'D', '1.0');")
conn.execute("INSERT INTO marks VALUES(11, 'D-', '0.7');")
conn.execute("INSERT INTO marks VALUES(12, 'F', '0.0');")

conn.execute("CREATE TABLE majors(mjr_id INTEGER, mjr_name char(50));")
conn.execute("INSERT INTO majors VALUES(1, 'Physics');")
conn.execute("INSERT INTO majors VALUES(2, 'Mathematics');")
conn.execute("INSERT INTO majors VALUES(3, 'Computer Science');")

conn.execute("CREATE TABLE courses(crs_id INTEGER, crs_name char(50));")
conn.execute("INSERT INTO courses VALUES(1, 'Math 1C');")
conn.execute("INSERT INTO courses VALUES(2, 'Comp Sci 100');")
conn.execute("INSERT INTO courses VALUES(3, 'Comp Sci 111');")
conn.execute("INSERT INTO courses VALUES(4, 'Math 1B');")
conn.execute("INSERT INTO courses VALUES(5, 'English 1');")
conn.execute("INSERT INTO courses VALUES(6, 'Comp Sci 2');")
conn.execute("INSERT INTO courses VALUES(7, 'Math 1A');")
conn.execute("INSERT INTO courses VALUES(8, 'History 10');")
conn.execute("INSERT INTO courses VALUES(9, 'Comp Sci 1');")

conn.execute("CREATE TABLE students("
             "stud_id char(50), stud_name char(50), "
             "stud_surname char(50), stud_majorid INTEGER,"
             "FOREIGN KEY(stud_majorid) REFERENCES majors(mjr_id));")
conn.execute("INSERT INTO students values('123-456-789', 'John', 'Doe', 3);")

conn.execute("CREATE TABLE student_courses(stdcr_id INTEGER, "
             "stdcr_studid char(50), stdcr_crsid INTEGER,"
             "FOREIGN KEY(stdcr_studid) REFERENCES students(stud_id),"
             "FOREIGN KEY(stdcr_crsid) REFERENCES courses(crs_id));")

conn.execute("INSERT INTO student_courses VALUES(1, '123-456-789', 1);")
conn.execute("INSERT INTO student_courses VALUES(2, '123-456-789', 2);")
conn.execute("INSERT INTO student_courses VALUES(3, '123-456-789', 3);")
conn.execute("INSERT INTO student_courses VALUES(4, '123-456-789', 4);")
conn.execute("INSERT INTO student_courses VALUES(5, '123-456-789', 5);")
conn.execute("INSERT INTO student_courses VALUES(6, '123-456-789', 6);")
conn.execute("INSERT INTO student_courses VALUES(7, '123-456-789', 7);")
conn.execute("INSERT INTO student_courses VALUES(8, '123-456-789', 8);")
conn.execute("INSERT INTO student_courses VALUES(9, '123-456-789', 9);")

conn.execute("CREATE TABLE student_courses_marks(stdcrmr_id INTEGER, "
             "stdcrmr_stdcrid INTEGER, stdcrmr_perid INTEGER, stdcrmr_markid INTEGER,"
             "FOREIGN KEY(stdcrmr_stdcrid) REFERENCES student_courses(stdcr_id),"
             "FOREIGN KEY(stdcrmr_perid) REFERENCES periods(per_id),"
             "FOREIGN KEY(stdcrmr_markid) REFERENCES marks(mark_id));")
conn.execute("INSERT INTO student_courses_marks VALUES(1, 1, 1, 3);")
conn.execute("INSERT INTO student_courses_marks VALUES(2, 2, 1, 1);")
conn.execute("INSERT INTO student_courses_marks VALUES(3, 3, 1, 2);")
conn.execute("INSERT INTO student_courses_marks VALUES(4, 4, 2, 4);")
conn.execute("INSERT INTO student_courses_marks VALUES(5, 5, 2, 1);")
conn.execute("INSERT INTO student_courses_marks VALUES(6, 6, 2, 2);")
conn.execute("INSERT INTO student_courses_marks VALUES(7, 7, 3, 2);")
conn.execute("INSERT INTO student_courses_marks VALUES(8, 8, 3, 3);")
conn.execute("INSERT INTO student_courses_marks VALUES(9, 9, 3, 1);")
conn.commit()
conn.close()



def get_students_transcriptions():
    """
    Function return json file with students transcriptions
    :return: json
    """
    reqconn = sqlite3.connect(DB_FILE)
    cursor = reqconn.execute("""Select
      st.stud_id,
      st.stud_name || ' ' || st.stud_surname "st_name",
      mj.mjr_name,
      cr.crs_name,
      mr.mark_name,
      pr.per_name,
      mr.mark_point
     From 
      students st,
      student_courses sc,
      courses cr,
      student_courses_marks scm,
      marks mr,
      periods pr,
      majors mj
    Where st.stud_id = sc.stdcr_studid
      and sc.stdcr_crsid = cr.crs_id
      and scm.stdcrmr_stdcrid = sc.stdcr_id
      and mr.mark_id = scm.stdcrmr_markid
      and st.stud_majorid = mj.mjr_id
      and pr.per_id = scm.stdcrmr_perid
    Order by st.stud_id, pr.per_id""")

    res = {}
    for row in cursor:
        if row[0] not in res:
            res[row[0]] = {'details': {'st_name': row[1], 'mjr_name': row[2]}, 'courses_marks': {}}

        mark = {'crs_name': row[3], 'mark_name': row[4], 'mark_point': row[6]}
        if row[5] not in res[row[0]]['courses_marks']:
            res[row[0]]['courses_marks'][row[5]] = [mark]
        else:
            res[row[0]]['courses_marks'][row[5]].append(mark)
    reqconn.close()
    return json.dumps(res)


def docalc(numbers, nlength=None):
    """
    Function return text with:
        range of list (min, max)
        missing numbers that not in list but in range of min and max values of list
        duplicates in list
    :param numbers: list of numbers
    :param nlength: length of list "numbers"
    :return: string
    """
    minval = numbers[0]
    maxval = minval
    ng_dic = {}
    dp_dic = {}
    res = ''
    for nm in numbers:
        if nm in ng_dic:
            if nm in dp_dic:
                dp_dic[nm] += 1
            else:
                dp_dic[nm] = 2
        else:
            ng_dic[nm] = nm

        if minval > nm:
            minval = nm

        if maxval < nm:
            maxval = nm

    res += 'Range is {} to {}\n'.format(minval, maxval)

    res += 'Missing Numbers:\n'

    val = minval
    while True:
        if val not in ng_dic:
            res += str(val)
            res += '\n'

        if val == maxval:
            break
        val += 1

    res += 'Duplicate Numbers:\n'
    for key in dp_dic:
        res += '{} appears {} times\n'.format(key, dp_dic[key])
    return res


class MyRequestHandler(SimpleHTTPRequestHandler):
    """
    Lite web server
    """
    def do_GET(self):
        """
        Create response on GET requests
        :return:
        """
        # index request
        if self.path == '/':
            self.path = '/home.html'

        # getting students transcriptions
        if self.path == '/gettranscription':
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(get_students_transcriptions())
            return

        # calculate list of numbers
        if 'docalc' in self.path:
            self.send_response(200)
            self.send_header('Content-Type', 'text/plain')
            self.end_headers()
            reqpar = parse_qs(urlparse(self.path).query)
            par = [int(s) for s in reqpar['in'][0].split(',')]
            self.wfile.write(docalc(par))
            return

        return SimpleHTTPRequestHandler.do_GET(self)

Handler = MyRequestHandler
server = SocketServer.TCPServer(('0.0.0.0', 8080), Handler)

server.serve_forever()






