#!/usr/bin/env python
def docalc(numbers, nlength=None):
    # nlength is not used but I have to create such input param because of requirements of test
    """
    Procedure print:
        range of list (min, max)
        missing numbers that not in list but in range of min and max values of list
        duplicates in list
    :param numbers: list of numbers
    :param nlength: length of list "numbers"
    """
    minval = numbers[0]
    maxval = minval
    ng_dic = {}
    dp_dic = {}
    for nm in numbers:
        if nm in ng_dic:
            if nm in dp_dic:
                dp_dic[nm] += 1
            else:
                dp_dic[nm] = 2
        else:
            ng_dic[nm] = nm

        if minval > nm:
            minval = nm

        if maxval < nm:
            maxval = nm

    print('Range is {} to {}'.format(minval, maxval))
    print('Missing Numbers:')

    val = minval
    while True:
        if val not in ng_dic:
            print(val)

        if val == maxval:
            break
        val += 1

    print ('Duplicate Numbers:')
    for key in dp_dic:
        print ('{} appears {} times'.format(key, dp_dic[key]))


print docalc([3, 1, -5, 3, 3, -5, 0, 10, 1, 1], 10)
